# Nonprofit XD Freebie

The website includes sections such as information about the organization, the issue of climate change, ways to take action, stories, upcoming events, and a subscription feature for newsletters.

### Description:
This website template is built using HTML and CSS.

### How to Use:
Open the index.html file in your web browser to view the website.
